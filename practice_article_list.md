[← back to 目录](HOME.md)

# 获取文章练习记录

获取文章练习记录

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_article_list
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| p_uid | int | 11 | 用户ID |
| offset_id | int | 11 | ID(APP的服务器ID字段) |
| update\_time | datetime | | 更新时间(APP的server\_update_time) |
| rows | int | 6 | 条数, 默认200 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "list": [
            {
                "id": 55, //APP存此字段请存成server_id
                "p_uid": 22,
                "scenario": 13,
                "workflow_id": 72,
                "article_id": 132,
                "read_article_time": 60,
                "duration_seconds": 15,
                "total_topic_nums": 13,
                "finished_nums": 13,
                "wrong_nums": 1,
                "created_time": "2018-01-18 12:00:11",
                "updated_time": "2018-01-18 12:00:11",
                "server_update_time": "2018-01-18 12:00:11",
                "status": 2
            },
            {
                "id": 55,
                "p_uid": 22,
                "scenario": 13,
                "workflow_id": 72,
                "article_id": 132,
                "read_article_time": 60,
                "duration_seconds": 15,
                "total_topic_nums": 13,
                "finished_nums": 13,
                "wrong_nums": 1,
                "created_time": "2018-01-18 12:00:11",
                "updated_time": "2018-01-18 12:00:11",
                "server_update_time": "2018-01-18 12:00:11",
                "status": 2
            }
        ]
    }
}
```
