[← back to 目录](HOME.md)

# 题目答对答错次数拉取

题目答对答错次数拉取

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer_count_get
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int |  |  |
| update_since | datetime |  |  |
| offset_id | int |  | 分页标识 上一页最后一条的id 默认0 |
| rows | int |  | 每页条数 默认300 |

### 响应

```json
{
	"code": 0,
	"message": "OK",
	"data": {
		"list": [{
			"ques_id": "5076",
			"wrong_times": "1",
			"right_times": "1"
		}, {
			"ques_id": "5077",
			"wrong_times": "1",
			"right_times": "1"
		}]
	}
}
```



