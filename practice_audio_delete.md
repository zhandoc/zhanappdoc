[← back to 目录](HOME.md)

# 优秀音频删除接口

优秀音频删除接口

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_audio_delete
```

参数

| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
| passport_id | int |☐ |  |
| question_id| int|☑ | 此项选填|
| audio_id| int|☑ | 音频ID |
| url| string|☑ | 音频链接 |

 * 注: audio_id、url二选一

### 响应

```json
{
    "code": 0,
    "message": "OK",
	"data": {
	}
}
```

如果失败
```json
{
    "code": -1,
    "message": "无删除权限，此音频不属于您",
    "data": {
    }
}
```
