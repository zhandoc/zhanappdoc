# 目录

## 此文档已不推荐查阅 请进入 [新文档地址](http://git.zhan.com/zhandoc/doc/blob/master/README.md#app) 查看

 * 需要找3楼的Fisher余开通 `git.zhan.com` 的权限
 * 然后才可以查看新文档
 * 此操作请在3月中旬之前完成，3.16日本文档将被Archive归档，外网不可访问
 
## 前言

* [接入指南](README.md)

## 接口

* [生成工作流](workflow_create.md)
* [结束工作流](workflow_terminate.md)
* [获取工作流（单套或全量）](workflow_get.md)
* [答题-记录列表](workflow_answer_list.md)
* [答题-记录列表-ipad用](workflow_answer_list_ipad.md)
* [答题-新增](workflow_answer.md)
* [题目答对答错次数拉取](workflow_answer_count_get.md)
* [题目答对答错次数提交](workflow_answer_count_post.md)
* [优秀音频点赞](practice_audio_vote.md)
* [优秀音频列表拉取](practice_audio_list.md)
* [优秀音频分享详情](practice_audio_share.md)
* [新旧题库映射](topic_map.md)
* [收藏题目](question_collect.md)
* [拉取错题本](answer_wrong.md)
* [文件上传（OSS直传）](upload_sts.md)
* [优秀音频删除 - JAVA老版本兼容用](practice_audio_delete.md)
* [提交文章练习记录](practice_article.md)
* [提交试卷练习记录](practice_paper.md)
* [获取文章练习记录](practice_article_list.md)
* [获取试卷练习记录](practice_paper_list.md)
* [时钟校对](timing.md)
* ~~[答题-更新](workflow_answer_modify.md)~~
* ~~[优秀音频递交](practice_audio_submit.md)~~
* ~~[获取答题记录（workflow型）](workflow_answer_list_multi.md)~~
* ~~[获取答题记录（非workflow型）](answer_list_multi.md)~~
* ~~[文件上传](upload.md)~~
* ~~[工作流进度 -- 未实现](workflow_progress.md)~~
* ~~[获取答题记录](workflow_answer_list_multi.md)~~
