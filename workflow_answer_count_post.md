[← back to 目录](HOME.md)

# 题目答对答错次数提交

题目答对答错次数提交

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer_count_post
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int |  |  |
| info | list | 300 | 详细请见 info 字段的说明 |



info字段

| 参数 | 类型 | 长度 | 描述 |
| :--- | :--- | :--- | :--- |
| ques_id |  |  |  |
| right_times |  |  |  |
| wrong_times |  |  |  |
| paper_id |  |  | 套题id |
| test_type |  |  | 考试类型 1-托福 2-雅思 |
| subspecialization |  |  | 题目的专项 1-听力,2-阅读,3-口语,4-写作 |
| time | datetime |  | 用户真实做题的时间 |



### 响应

```json
{
    "code": 0, // 若失败，此非0
    "message": "OK",
    "data": {
    }
}
```

### MOCK

```
POST http://api-admin-corpus.zhan.com/zapp/workflow_answer_count_update

passport_id=1111&info[0][ques_id]=1&info[0][right_times]=0&info[0][wrong_times]=100&info[0][paper_id]=1&info[0][test_type]=1&info[0][subspecialization]=1&info[0][time]=2017-08-08%2010:03:03
```



