[← back to 目录](HOME.md)

# 新旧题库映射(TOPIC映射)

新旧题库映射(TOPIC映射)


### 请求

...

URL

```
http://api-admin-corpus.zhan.com/zapp/topic_map
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| question | list| | 请查看 `question字段定义` |
| type| string| | 类型 1旧转新2新转旧 |
| data| string | | {question:[question1, question2...同question字段], type: 1} |

question字段定义

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| paper_name | string| | 小写 `tpo2`/`extra1` |
| passage| string| | 小写 `reading`/`listening`/`speaking`/`writing` |
| seqno| int | | type=1时传旧库的question_no type=2时传语料库的seqno |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
       "mapping": {
            "tpo5-listening-21": {
                "paper_name": "tpo5",
                "passage": "listening",
                "seqno": 4
            }
       }
    }
}
```

当不存在时，则

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "mapping": {
            "tpo5-listening-21": null
        }
    }
}
```

### MOCK

```
http://api-admin-corpus.zhan.com/zapp/topic_map?type=1&question%5B0%5D%5Bpaper_name%5D=tpo5&question%5B0%5D%5Bpassage%5D=listening&question%5B0%5D%5Bseqno%5D=21
```