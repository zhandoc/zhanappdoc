[← back to 目录](HOME.md)

# 结束工作流

结束工作流

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_terminate
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| workflow_id | list |  |eg: [10010,10086]  |
| passport_id | int |  |  |


### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
    }
}
```

### Mock
```
POST /zapp/workflow_terminate HTTP 1.1

workflow_id[]=10010&workflow_id[]=10086&passport_id=3158
```