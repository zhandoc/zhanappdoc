[← back to 目录](HOME.md)

# 生成工作流

生成工作流

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_create
```

参数

| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
|id |int | ☐ | 本地id|
|p_uid |int  |☐ | passport 登录用户ID|
|student_id |int  |☑ | 学生ID|
|appid |string  |☐ | 来源:`beikao` / `app` / `mokao` / `lms` / `zhantuan` / `mokao` / `app_pad` APP请写死`app`|
|scenario |int  |☐ | 场景:11`模考` / 12`单科模考` / 13`按篇练` / 14`按题型练` / 15`错题本练习` / 16`收藏夹` / 17`答题记录列表` |
|scenetype|int  |☑ | 场景type: 1`场景` / 2`顺序` |
|practice_type |int  |☐ | 类型:0`整卷` / 1`阅读` / 2`听力` / 3`口语` / 4`写作` |
|ques_practice_type |tinyint  |☐ | scenario=14时,按题型练,1智能练习,2自主练习|
|paper_id |int  |☐ | 试卷ID|
|article_id |int  |☐ | 文章ID|
|createtime |datetime  |☐ |创建时间 |
|deleted|int  |☐ |删除态（清空答题记录时候，操作此字段） |
|deletetime|datetime  |☐ |删除时间 |
|status |int  |☐ | 状态:0未完成/1已完成/9已删除|


### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "workflow": {
            "la-193": 7034410 // 成功返回工作流 id
        }
    }
}
```

### MOCK

 - 批量

```
http://api-admin-corpus.zhan.com/zapp/workflow_create?passport_id[]=1&paper_id[]=1&id[]=local10&passport_id[]=1&paper_id[]=1&id[]=local11

{
    "code": 0,
    "message": "OK",
    "data": {
        "workflow": {
            "local10": "6807151",
            "local11": "6807152"
        }
    }
}
```

 - 非批量

```
http://api-admin-corpus.zhan.com/zapp/workflow_create?passport_id=1&paper_id=1&...

{
    "code": 0,
    "message": "OK",
    "data": {
        "workflow": "6807152"
    }
}
```


