# 创建工作流所需字段


### 11 模考   

| 字段 | 示例(null为不传) |
|---|---|
|workflow_id | 3799597 |
|p_uid | 5183163 |
|student_id | 12646390 |
|app_id | beikao |
|scenario | 11 |
|practice_type | 0 |
|ques_practice_type | NULL |
|paper_id | 2 |
|article_id | NULL |
|createtime | 2017-12-23 13:08:58 |
|updatetime | 2017-12-23 13:08:58 |
|status | 0 |


### 12 单科模考 

| 字段 | 示例(null为不传) |
|---|---|
|workflow_id | 3799658 |
|p_uid | 6854937 |
|student_id | 0 |
|app_id | beikao |
|scenario | 12 |
|practice_type | 2 |
|ques_practice_type | NULL |
|paper_id | 46 |
|article_id | NULL |
|createtime | 2017-12-23 13:18:45 |
|updatetime | 2017-12-23 13:18:45 |
|status | 0 |


### 13 按篇练    

| 字段 | 示例(null为不传) |
|---|---|
|workflow_id | 3799691 |
|p_uid | 5054512 |
|student_id | 13356237 |
|app_id | beikao |
|scenario | 13 |
|practice_type | 1 |
|ques_practice_type | NULL |
|paper_id | 44 |
|article_id | 713 |
|createtime | 2017-12-23 13:23:10 |
|updatetime | 2017-12-23 13:23:10 |
|status | 0 |


### 14 按题型练   

| 字段 | 示例(null为不传) |
|---|---|
|workflow_id | 3799689 |
|p_uid | 1566498 |
|student_id | 5534575 |
|app_id | beikao |
|scenario | 14 |
|practice_type | 1 |
|ques_practice_type | NULL |
|paper_id | NULL |
|article_id | NULL |
|createtime | 2017-12-23 13:23:03 |
|updatetime | 2017-12-23 13:23:03 |
|status | 0 |


### 15 错题本练习    

| 字段 | 示例(null为不传) |
|---|---|
|workflow_id | 3799637 |
|p_uid | 3778188 |
|student_id | 7320962 |
|app_id | beikao |
|scenario | 15 |
|practice_type | 1 |
|ques_practice_type | NULL |
|paper_id | NULL |
|article_id | NULL |
|createtime | 2017-12-23 13:15:51 |
|updatetime | 2017-12-23 13:16:17 |
|status | 1 |


### 16 收藏夹    

| 字段 | 示例(null为不传) |
|---|---|
|workflow_id | 3789869 |
|p_uid | 3864599 |
|student_id | 11161631 |
|app_id | beikao |
|scenario | 16 |
|practice_type | 1 |
|ques_practice_type | NULL |
|paper_id | NULL |
|article_id | NULL |
|createtime | 2017-12-22 10:40:56 |
|updatetime | 2017-12-22 10:40:56 |
|status | 0 |