[← back to 目录](HOME.md)

# 提交试卷练习记录

提交试卷练习记录

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_paper
```


参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| data | string | NaN | 字符串数据,格式为`"[{数据1},{数据2}...]"` |


data下的key

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| id |int | 11 | |
| workflow_id |int | 11 |  |
| p_uid |int | 11 | 登录用户 passport id |
| paper_id |int | 11 | |
| listen_score |mediumint | 8 |  得分 |
| read_score |mediumint | 8 |  得分 |
| speak_score |mediumint | 8 | |
| write_score |mediumint | 8 | |
| reading_use_time |int | 11 | 阅读用时正计时(秒) |
| listening_use_time |int | 11 | 听力用时正计时（秒） |
| speaking_use_time |int | 11 | 口语用时正计时（秒） |
| writing_use_time |int | 11 | 写作答题用时正计时 |
| status |tinyint | 2 |  0:进行中 1：已完成 |
| created_time |datetime | | 创建时间  YYYY-mm-DD HH:ii:ss |
| updated_time |datetime | | 更新时间  YYYY-mm-DD HH:ii:ss |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
    }
}
```
