[← back to 目录](HOME.md)

# 文件上传（OSS直传）

文件上传（OSS直传）


### 请求

获取临时策略和临时签名

URL

```
http://api-admin-corpus.zhan.com/zapp/upload_sts
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | | 必需要已登录才允许上传文件|

### 响应

```json
{
	"code": 0,
	"message": "OK",
	"data": {
	    "endpoint": "zhancorpus.oss-cn-hangzhou.aliyuncs.com",
	    "accessid": "STS.3p80Aho1ek0Jm63XMh",
	    "policy": "EUOIhJMQBMjRywXq7MQ/cjLYg80Aho1ek0Jm0A0Jm63XMhr9Oc5scjLYg80AhEUOIhJMQBMcjLYg80Aho1ek0Jm0A0Jm63XMhr9Oc5scjLYg80AhEUOIhJMQBM",
	    "signature": "Xqo1ek0Jm63XMhr9Oc5s",
	    "expires_in": 1712,   // 多久过期 秒
	    "prefix": "usercontents/2017/11/16/18313/20171116103312T09"  // 客户端的上传的文件的命名必须以此开头
	}
}
```

当失败时

```json
{
	"code": -1,
	"message": "服务器内部错误",
	"data": {
	}
}
```

客户端请求步骤:
 - 将响应的endpoint前面拼上`https://`
 - 参照下图向`https://endpoint`发出`Content-Type=multipart/form-data`的`post`请求。文件名必须为`prefix`+随机6位数字+扩展名。
 - OSS会返回HTTP CODE=20x的响应。当响应码非20x则上传失败，否则则成功，走后续业务逻辑。


客户端请求示例：

[![QQ截图20171116110851.png](https://i.loli.net/2017/11/16/5a0d00d343932.png)](https://i.loli.net/2017/11/16/5a0d00d343932.png)

