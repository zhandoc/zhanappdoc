[← back to 目录](HOME.md)

# 优秀音频拉取接口

优秀音频拉取接口

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数 分页只对他人的音频有效 第一页才加载自己的音频

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_audio_list
```

参数

| 参数 | 类型 | 可选 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | no| 如果未登录，传null |
| question_id| int| no| |
| rows| int| yes| 分页单页条数|
| offset_id| int| yes| 分页从哪一条开始 如果此项非Null非0 则不返回self|

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "self": [
            {
                "audio_id": 10086,
                "passport_id": 10212,
                "nickname": "肛裂的乌龟", // 语料库接口无此数据统一返回Null由JAVA接口补全
                "avatar": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.jpg",  // 语料库接口无此数据统一返回Null由JAVA接口补全
                "audio": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.mp3",
                "audio_length": 48,
                "created_at": "2017-01-01 13:01:02",
                "praise_count": 18,
                "trampele_count": 0
            }, 
            {
                "audio_id": 10089,
                "passport_id": 10212,
                "nickname": "肛裂的乌龟",
                "avatar": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.jpg",
                "audio": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.mp3",
                "audio_length": 48,
                "created_at": "2017-01-01 13:01:02",
                "praise_count": 18,
                "trampele_count": 0
            }
        ],
        "others": [
            {
                "audio_id": 441,
                "passport_id": 10213,
                "nickname": "弗洛伊德",
                "avatar": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.jpg",
                "audio": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.mp3",
                "audio_length": 48,
                "created_at": "2017-03-01 13:01:02",
                "praise_count": 18,
                "trampele_count": 0,
                "praised_by_me": 1 // 我已点赞
            }, 
            {
                "audio_id": 442,
                "passport_id": 10214,
                "nickname": "亚里士多德",
                "avatar": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.jpg",
                "audio": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.mp3",
                "audio_length": 48,
                "created_at": "2017-09-01 13:01:02",
                "praise_count": 18,
                "trampele_count": 0,
                "praised_by_me": 1 // 我已点赞
            }
        ]
    }
}
```
