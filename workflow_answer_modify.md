[← back to 目录](HOME.md)

# 答题-更新

答题-更新

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer_modify
```


| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | N | |
| workflow_id | int | Y | 语料库practice_id |
| scenario | int | Y | 11 模考, 12单科模考,13按篇练,14按题型练,15错题本练习,16 收藏夹 |
| paper_id | int | Y | 卷子ID |
| tpo_no | string| Y | TPO编号，paperID |
| phase_id | int | Y | 文章序号(托福套卷用) |
| ques_no | int | Y | 题目序号(套卷中的序号) |
| ques_id | int | Y | 题目编号(题目唯一编号) |
| tmp_id | int | Y | 临时ID(游客ID等) |
| guid | int | Y | LMS用户ID |
| subspecialization | int | N | 题目的专项(1 阅读 2 听力 3 口语 4听力) |
| test_type | int | Y | 对应的考试类别(1: 托福 2:雅思) |
| is_official | int | Y | 是否是真题(0: 否 1:是) |
| is_jj | int | Y | 是否是机经题(0: 否 1:是) |
| answer | string | Y | 答题答案 如果为音频URL 参数带?deleted 语料库API则会将此答题音频从优秀音频移除 |
| answer_ext | string | Y | 答题附加答案 |
| result | int | Y | 答题的结果0:正确1:错误2:其它诸如半对、半错的特殊结果 |
| practice_datetime | date | Y | 答题时间 |
| used_time | date | Y | 答题使用时间(秒) |
| practice_ip_addr | string | Y | 答题时所用的IP地址 |
| practice_mac_addr | string | Y | 答题时所用客户端网卡的物理地址 |
| retry_times | int | Y | 重答次数(在一次做题过程中对同一题修改答案的次数) |
| update_datetime| timestamp | N| 插入时间 APP本地时间 时间戳格式 eg: 1511252994200 到ms级 |

### 响应

```json
{
	"code": 0,
	"message": "OK",
	"data": {
	    "affected": 1 //更新成功行数  如果没有受影响的返回 `0`
	}
}
```


