[← back to 目录](HOME.md)

# 优秀音频递交

优秀音频递交

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_audio_submit
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | | 如果未登录，传null |
| question_id| int| | 此项选填|
| audio| string| | 音频URL 请先走文件上传接口获得URL |
| audio_length| decimal| | 音频长度 单位秒|
| created_at| datetime| | 客户端传的用户录音时间|

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
	    "audio_id": 10086
	}
}
```
