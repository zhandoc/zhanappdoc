# 获取答题记录（非workflow型）

获取答题记录（非workflow型）

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/answer_list_multi
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport\_id | int |  |  |
| update\_time | datetime |  |  |
| offset\_id | int |  | 分页标识 上一页最后一条的id 默认0 |
| rows | int |  | 每页条数 默认300 |

### 响应

```json
{
    code: 0,
    message: "OK",
    data: {
        action: 1 //操作成功1 失败0
    }
}
```



