[← back to 目录](HOME.md)

# 优秀音频分享详情

优秀音频分享详情

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_audio_share
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| audio_id| int| | |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "audio_id": 10086,
        "passport_id": 10212,
        "nickname": "肛裂的乌龟",
        "avatar": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.jpg",
        "audio": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.mp3",
        "audio_length": 48,
        "created_at": "2017-01-01 13:01:02",
        "praise_count": 18,
        "trampele_count": 0,
        "task_name": "Conversation 4",
        "question": {
            "question_id": 10086,
            "question_title": "尼姑拉斯.凯奇",
            "question_content": "他是烂片之王."
        },
        "paper": {
            "paper_id": 3158,
            "paper_name": "tpo10"
        }
    }
}
```
