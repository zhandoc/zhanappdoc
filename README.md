[← back to 目录](HOME.md)

** 语料库接口必传参数，具体接口文档不再说明 ：**

| 参数        |  类型 |    说明  |
|  --------  |  -------- |-------- |
| appID|  string | app,app_pad,pc,zhantuan,beikao,mokao,lms
|deviceID| string |设备号 
|model|string| 客户端 型号 
|os|int|0:android;1:ios;2:pc
|version|int|客户端版本号（1001001 对应版本号 1.1.1）
|timezone|int|时区， 中时区（0时区）、东1-12区（1~12），西1-12区（-1~-12）
|timestamp| int|  时间戳 