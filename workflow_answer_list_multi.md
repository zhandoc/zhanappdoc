[← back to 目录](HOME.md)

# 获取答题记录

获取答题记录

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer_multi
```

参数

| 参数 | 类型 | 可选 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | No |  |
| workflow_id| int | Yes | 若无可不传或传0 |
| update_time | datetime| Yes |  |
| offset_id| int | Yes |分页标识 上一页最后一条的id 默认0 |
| rows| int| Yes |每页条数 默认300 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "action": 1 //操作成功1 失败0
    }
}
```



