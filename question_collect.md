[← back to 目录](HOME.md)

# 收藏题目接口

收藏题目接口

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/collect
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | | 如果未登录，传null |
| question_id| int| | 此项选填|
| question_type| int| | 问题类型 1阅读2听力3口语4写作 |
| tpo_id| int| | 套题id |
| collected| int| | 是否收藏 1是-1取消 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
    }
}
```

如果失败
```json
{
    "code": -1,
    "message": "您已收藏过此问题",
    "data": {
    }
}
```
