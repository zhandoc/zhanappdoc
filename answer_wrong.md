[← back to 目录](HOME.md)

# 拉取错题本

拉取错题本

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/answer_wrong
```

参数

| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | N | 用户id |
| ~~offset_id~~| ~~int~~| ~~Y~~ | ~~位置id，前一页最后一条的id，如果第一页，此传0或不传均可~~|
| update_time | datetime | Y | YYYY-mm-dd HH:ii:ss 格式 默认不传 |
| rows| int| Y | 条数，默认300条（如果最后一条的时间存在多条记录，会返回这个时间的所有记录） |

### 响应

```json
{
	"code": 0,
	"message": "OK",
	"data": {
        "list": [
            {
                "id": "37",
                "paper_id": "66",
                "ques_id": "1551",
                "test_type": 2,
                "subspecialization": "2",
                "p_uid": "1325047",
                "is_new_resource": 1,
                "is_conquer": 0,                                               // (0:未攻克 1：已攻克)
                "status": 0,
                "wrong_times": 1,
                "practice_datetime": "2017-12-15 14:40:02",
                "created_at": "2017-12-15 14:40:02",
                "updated_at": "2017-12-15 14:40:02",
                "app_id": "beikao",
                "is_official": 1,
                "scenario": 21
            },
            {
                "id": "38",
                "paper_id": "65",
                "ques_id": "1506",                                              // tbl_toefl_topic.id
                "test_type": 2,
                "subspecialization": "2",
                "p_uid": "1325047",
                "is_new_resource": 1,
                "is_conquer": 0,
                "status": 0,
                "wrong_times": 1,
                "practice_datetime": "2017-12-18 19:20:31",
                "created_at": "2017-12-18 19:20:31",
                "updated_at": "2017-12-18 19:20:31",
                "app_id": "beikao",
                "is_official": 1,
                "scenario": 21
            }
        ],
        "has_next": true
    }
}
```

has_next => TRUE还有更多/FALSE没有了