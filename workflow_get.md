[← back to 目录](HOME.md)

# 获取工作流（单套或全量）

获取工作流（单套或全量）

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_get
```

参数

| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | ☐|用户id |
| scenario | list[int] | ☐|场景id，传多个|
| update_time | datetime | ☐||
| offset_id | int | ☐| 上一页最后一条的id 第一页请传0|
| rows | int | ☐| 一页多少条 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "workflow_list": [
            {
                "workflow_id": 12306,
                "p_uid": 13311,
                "student_id": null,
                "app_id": "app",
                "scenario": 14,
                "scenetype": 1,
                "practice_type": 4,
                "ques_practice_type": 2,
                "paper_id": 31,
                "article_id": 1451,
                "createtime": "2017-11-24 13:31:22",
                "updatetime": "2017-11-24 13:31:22",
                "deleted": 0,
                "deletetime": null,
                "status": 0
            },
            {
                "workflow_id": 12307,
                "p_uid": 13311,
                "student_id": null,
                "app_id": "app",
                "scenario": 14,
                "scenetype": 1,
                "practice_type": 4,
                "ques_practice_type": 2,
                "paper_id": 31,
                "article_id": 1452,
                "createtime": "2017-11-24 13:31:22",
                "updatetime": "2017-11-24 13:31:22",
                "deleted": 0,
                "deletetime": null,
                "status": 0
            },
            {
                "workflow_id": 12308,
                "p_uid": 13311,
                "student_id": null,
                "app_id": "app",
                "scenario": 14,
                "scenetype": 2,
                "practice_type": 4,
                "ques_practice_type": 2,
                "paper_id": 31,
                "article_id": 1453,
                "createtime": "2017-11-24 13:31:22",
                "updatetime": "2017-11-24 13:31:22",
                "deleted": 1,
                "deletetime": "2017-11-24 15:31:22",
                "status": 0
            }
        ]
    }
}
```



