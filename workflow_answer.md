[← back to 目录](HOME.md)

# 答题-新增

答题-新增

#### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer
```

参数

| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
| data | string | N | json化的字符串 见 `data结构` |

data结构

| 参数 | 类型 | 选填 | 描述 |
| --- | --- | --- | --- |
| appid | int | N | 写死成`app` |
| passport\_id | int | N | 用户ID|
| workflow\_id | int | Y | 语料库practice\_id |
| scenario | int | N | 11 模考, 12单科模考,13按篇练,14按题型练,15错题本练习,16 收藏夹 |
| paper\_id | int | N | 卷子ID |
| tpo_no | string| N | TPO编号，paperID |
| phase\_id | int | N | 文章序号(托福套卷用) |
| ques_no | int | N | 题目序号(套卷中的序号) |
| ques\_id | int | N | tbl_toefl_topic的id |
| btype | int | N | 题型(1：阅读 2：听力 3：口语 4：写作) |
| tmp\_id | int | Y | 临时ID(游客ID等) |
| guid | int | Y | LMS用户ID |
| subspecialization | int | N | 题目的专项(1 阅读 2 听力 3 口语 4听力) |
| test_type | int | Y | 对应的考试类别(1: 托福 2:雅思) |
| is_official | int | Y | 是否是真题(0: 否 1:是) |
| is_jj | int | Y | 是否是机经题(0: 否 1:是) |
| answer | string | N | 答题答案 |
| answer_ext | string | Y | 答题附加答案 |
| result | int | N | 答题的结果0:正确1:错误2:其它诸如半对、半错的特殊结果 |
| practice_datetime | date | N | 答题时间 |
| used_time | int | N | 答题使用时间(秒) ，当btype为3时，请传录音时长|
| practice\_ip_addr | string | N | 答题时所用的IP地址 |
| practice\_mac_addr | string | N | 答题时所用客户端网卡的物理地址 |
| retry_times | int | N | 重答次数(在一次做题过程中对同一题修改答案的次数) |
| create_datetime| timestamp | N| 插入时间 APP本地时间 时间戳格式 eg: 1511252994 精确到秒|
| before\_id | string | Y| 基于什么id的变动 如果新做题则传null或不传 |
| is_finished | int | N| 工作流是否就此结束 0未结束/1已结束|

多条, 请求格式如下

```
data=[{"passport_id":13124,"workflow_id":null,"scenario":11,"paper_id":3},...]
```
### 响应

```json
{
	"code": 0,
	"message": "OK",
	"data": {
	}
}
```

### Mock Payload
```JavaScript
$.ajax({
    url: 'http://api-admin-corpus.zhan.com/zapp/workflow_answer',
    type: 'POST',
    contentType: "application/json",
    data: JSON.stringify([{
        workflow_id:1,
        answer:33,
        result:0,
        practice_datetime:'2017-01-01',
        used_time:2,
        passport_id:33,
        update_datetime:1,
        used_time:1,
        ques_id:55,
        paper_id:11
    }])
});
```

### 请求报文截图

![1.png](https://i.loli.net/2017/11/27/5a1bbb31a72d1.png)

![2.png](https://i.loli.net/2017/11/27/5a1bbb31ae8e4.png)
