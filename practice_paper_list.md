[← back to 目录](HOME.md)

# 获取试卷练习记录

获取试卷练习记录

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_paper_list
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| p_uid | int | 11 | 用户ID |
| offset_id | int | 11 | ID(APP的服务器ID字段) |
| update\_time | datetime | | 更新时间(APP的server\_update_time) |
| rows | int | 6 | 条数, 默认200 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "list": [
            {
                "id": 55, //APP存此字段请存成server_id
                "workflow_id": 22,
                "p_uid": 13,
                "paper_id": 72,
                "listen_score": 132,
                "read_score": 60,
                "speak_score": 15,
                "write_score": 13,
                "reading_use_time": 13,
                "listening_use_time": 1,
                "speaking_use_time": 1,
                "writing_use_time": 3,
                "status": 1,
                "created_time": "2018-01-18 12:00:11",
                "updated_time": "2018-01-18 12:00:11",
                "server_update_time": "2018-01-18 12:00:11"
            },
            {
                "id": 56,
                "workflow_id": 22,
                "p_uid": 13,
                "paper_id": 72,
                "listen_score": 132,
                "read_score": 60,
                "speak_score": 15,
                "write_score": 13,
                "reading_use_time": 13,
                "listening_use_time": 1,
                "speaking_use_time": 1,
                "writing_use_time": 3,
                "status": 1,
                "created_time": "2018-01-18 12:00:11",
                "updated_time": "2018-01-18 12:00:11",
                "server_update_time": "2018-01-18 12:00:11",
            },
        ]
    }
}
```
