[← back to 目录](HOME.md)

# 提交文章练习记录

提交文章练习记录

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_article
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| data | string | NaN | 字符串数据,格式为`"[{数据1},{数据2}...]"` |


data下的key

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| id |int | 11 |  |
| p_uid |int | 11 |登录用户 passport id
| scenario |int | 4 |场景： 11 模考, 12单科模考,13按篇练,14按题型练,15错题本练习
| workflow_id |int | 11 |workflow id
| article_id |int | 11 | |
| read_article_time |int | 11 |阅读文章材料时间,阅读和写作需要传|
| duration_seconds |int | 11 |答题用时,不包括阅读材料时间
| total_topic_nums |int | 4 |article包含的题目数量，必传
| finished_nums |int | 4 |已完成题数  |
| wrong_nums |int | 4 |错误题数 |
| status |int | 2 | 0-未开始 1-进行中 2-已结束 请不要使用0的状态 |
| created_time |datetime| |  创建时间 YYYY-mm-DD HH:ii:ss |
| updated_time |datetime| | 更新时间 YYYY-mm-DD HH:ii:ss|

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
    }
}
```
