[← back to 目录](HOME.md)

# APP老数据迁移

### 备忘
  1.  迁移脚本 场景 按篇

### 涉及表
  - tbl\_toefl\_workflow
  - tbl\_ques\_result
  - tbl\_ques\_result\_history
  - ~~tbl\_ques\_wrong~~
  - ~~tbl\_toefl\_topic\_count~~

### 流程
  - 新建工作流
    - 一个用户只开启一条
    - 迁移过程中建立临时表?
        1. 从tmp\_app\_migration\_workflow中读取用户有没有工作流 若无则插入
            - tmp\_app\_migration_workflow
                1. p_uid       int   pk
                2. workflow_id int
    - 如果不建临时表 可以workflow表往后自增600w `passport_id` + 600万 即认为是 `workflow_id` 先插入答题记录 再产生workflow
  - 插入答题记录
    - 一次读取1000条(chunk, 减轻网络传输耗时)
    - 过滤掉无效用户
    - 题号转换(若转换失败的，记录下来，临时表记录?)
        1. 从appDb中读取答题记录. limit 
            - app也做了分表，也是按用户分的，所以可以依次跑，无先后的问题
            - 取出来对User_id做判断 如果!is_numeric 则跳过
            - 通过题目映射表转换题号
  - 插入答题变动记录
  - ~~更新错题本~~
  - ...
