[← back to 目录](HOME.md)

# 文件上传

文件上传


### 请求

此接口只支援POST，且需要使用FORM FILE二进制文件流方式（非URL_ENCODED方式）传输所有数据

URL

```
http://api-admin-corpus.zhan.com/zapp/upload
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | | 必需要已登录才允许上传文件|
| mime | string| | 文件的元信息中的Mime-Type，如: image/jpeg, audio/mp3 |
| file | blob | | 文件 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
           "url": "https://file-corpus.zhan.com/dev/image/282430_ad53266e42030f4e839bd8cbcbeb472b1509627908.mp3" 
    }
}
```

当失败时

```json
{
    "code": -1,
    "message": "连接对象存储服务超时",
    "data": {
    }
}
```

### Mock

```
$ curl -F 'passport_id=1' -F'mime=image/jpeg' -F 'file=@1499676168904466.jpg' http://server.natappfree.cc:34633/zapp/upload -v
*   Trying 120.24.49.3...
* TCP_NODELAY set
* Connected to server.natappfree.cc (120.24.49.3) port 34633 (#0)
> POST /zapp/upload HTTP/1.1
> Host: server.natappfree.cc:34633
> User-Agent: curl/7.56.0
> Accept: */*
> Content-Length: 205050
> Content-Type: multipart/form-data; boundary=------------------------37cb1aa77e260818
> Expect: 100-continue
>
< HTTP/1.1 100 Continue
< HTTP/1.1 200 OK
< Date: Tue, 07 Nov 2017 05:52:12 GMT
< Server: Apache/2.4.27 (Win32) OpenSSL/1.0.2l PHP/7.0.23
< X-Powered-By: PHP/7.0.23
< Access-Control-Request-Method: POST,GET,OPTIONS
< Access-Control-Allow-Credentials: true
< Content-Length: 158
< Content-Type: application/json; charset=UTF-8
<
{"code":0,"message":"OK","data":{"url":"https://file-corpus.zhan.com/dev/image/282467_648a2ca1c043a351376d9c21da2d302d1510033934.jpg","attachment_id":282467}}* Connection #0 to host server.natappfree.cc left intact
```