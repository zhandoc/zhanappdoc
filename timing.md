[← back to 目录](HOME.md)

# 时钟校对

时钟校对

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/timing
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| time | string | NaN | 将设备时间转换成北京时候后的timestr |


### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "servertime": "2018-02-09 15:49:21",
        "inaccuracy": 60    //  响应的如果为60表示客户端需要在自身时间基础上加60秒 如果为-60表示需要减60秒
    }
}
```
