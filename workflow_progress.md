[← back to 目录](HOME.md)

# 工作流进度

工作流进度

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_progress
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int |  |  |
| paper_id | int |  |  |


### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "tpo45": [
            {
                "btype": 1,       // 1-阅读 2-听力 3-口语 4-写作
                "phase_id": 1,
                "progress": 0.15, // 进度 请自行转换成%
                "last_seqno": 1   // 断点续做
            },
            {
                "btype": 1,
                "phase_id": 2,
                "progress": 0.15,
                "last_seqno": 1
            },
            {
                "btype": 2,
                "phase_id": 1,
                "progress": 0.9,
                "last_seqno": 1
            }
        ]
    }
}
```



