[← back to 目录](HOME.md)

# 答题-记录列表

答题-记录列表

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer_list
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int |  | 用户id |
| offset_id | int |  | 分页标识 上一页最后一条的id 默认0 |
| rows | int |  | 每页条数 默认300 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
        "has_next": 1,
        "list": [
            {
                "id": "168",
                // ID
                "practice_id": "6751258",
                // 答题编号(作业ID workflow id)
                "scenario": "0",
                // 答题场景(1:模考 2:作业 3:练习 )
                "paper_id": "4",
                // 卷子ID
                "tpo_no": null,
                // TPO编号
                "phase_id": null,
                // 文章序号(托福套卷用)
                "ques_no": null,
                // 题目序号(套卷中的序号)
                "ques_id": "126",
                // TOPIC ID
                "p_uid": "3000541",
                // PASSPORT USER ID
                "tmp_id": null,
                // 临时ID(游客ID等)
                "guid": null,
                // LMS用户ID
                "subspecialization": "1",
                // 题目的专项(听力,阅读...)
                "test_type": "1",
                // 对应的考试类别(1: 托福 2:雅思)
                "is_official": null,
                // 是否是真题(0: 否 1:是)
                "is_jj": null,
                // 是否是机经题(0: 否 1:是)
                "answer": null,
                // 答题答案
                "answer_ext": null,
                // 答题附加答案
                "result": "1",
                // 答题的结果(0: 正确 1:错误 2:其它 诸如半对 半错的特殊结果)
                "practice_datetime": "2017-01-17 17:55:59",
                // 答题时间
                "used_time": "0",
                // 答题使用时间(单位:秒)
                "practice_ip_addr": null,
                // 答题时所用的IP地址
                "practice_mac_addr": null,
                // 答题时所用客户端网卡的物理地址
                "retry_times": null,
                // 重答次数(在一次做题过程中对同一题修改答案的次数)
                "server_ip": null,
                // 同步语料库的应用服务器IP
                "app_id": null,
                // 平台来源
                "is_new_resource": "1",
                // 题库是否是使用语料库(0:否 1:是)
                "create_datetime": "1511252994",
                // 创建时间 时间戳格式
                "update_datetime": null,
                // 更新时间
                "status": "0" // 状态(0:正常 1：删除)
            },
            {
                "id": "169",
                "practice_id": "6751260",
                "scenario": "0",
                "paper_id": "4",
                "tpo_no": null,
                "phase_id": null,
                "ques_no": null,
                "ques_id": "126",
                "p_uid": "3000541",
                "tmp_id": null,
                "guid": null,
                "subspecialization": "1",
                "test_type": "1",
                "is_official": null,
                "is_jj": null,
                "answer": null,
                "answer_ext": null,
                "result": "1",
                "practice_datetime": "2017-01-17 17:56:13",
                "used_time": "0",
                "practice_ip_addr": null,
                "practice_mac_addr": null,
                "retry_times": null,
                "server_ip": null,
                "app_id": null,
                "is_new_resource": "1",
                "create_datetime": "1511252994",
                "update_datetime": null,
                "status": "0"
            }
        ]
    }
}
```

### MOCK

```
http://api-admin-corpus.zhan.com/zapp/workflow_answer_list?passport_id=3000541&offset_id=167&rows=2
```



