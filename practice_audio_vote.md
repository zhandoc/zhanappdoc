[← back to 目录](HOME.md)

# 优秀音频点赞接口

优秀音频点赞接口

### 请求

请求支持POST或GET，但参数尽量要么全在GET参数要么全在POST参数

URL

```
http://api-admin-corpus.zhan.com/zapp/practice_audio_vote
```

参数

| 参数 | 类型 | 长度 | 描述 |
| --- | --- | --- | --- |
| passport_id | int | | 如果未登录，传null |
| question_id| int| | 此项选填|
| audio_id| int| | 音频ID |
| status| int| | 点赞状态:1点赞,-1取消赞,大于1增加N个赞 |

### 响应

```json
{
    "code": 0,
    "message": "OK",
    "data": {
	}
}
```

如果失败
```json
{
    "code": -1,
    "message": "您已赞过本音频",
    "data": {
    }
}
```
